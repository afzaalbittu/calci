
package com.snapshot.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 * Unit test for Calculator Class.
 */
public class CalculatorTest
{
	private Calculator calc;
	
    @Before
	public void setUp() throws Exception {
		calc = new Calculator();
	}
	
	@After
	public void tearDown() throws Exception {
		calc = null;
	}
	
	@Test
	public void testAdd() {
		double result = calc.add(100, 100);
		assertEquals(200, result, 0);
	}
	
	@Test
	public void testSubtract() {
		double result = calc.subtract(100, 100);
		assertEquals(0, result, 0);
	}
	
	@Test
	public void testMultiply() {
		double result = calc.multiply(2, 100);
		assertEquals(200, result, 0);
	}
	
	@Test
	public void testDivide() {
		double result = calc.divide(100, 100);
		assertEquals(1, result, 0.001);
	}
	
	@Test(expected = ArithmeticException.class)
	public void testDivideByZero() {
		double result = calc.divide(100, 0);
	}
}
